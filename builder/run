#!/bin/bash

#set -x
#set -eo pipefail

export PATH="$PATH:/opt/app-root/downloads:/opt/app-root/bin"

# AutoUpdate WP Cli
/opt/app-root/bin/wp cli update --yes

# ------- EO 03.04.2019 -------
if ! $(/opt/app-root/bin/wp core is-installed); then
    ###################### New install ######################
    echo "Wordpress download and install"
    /opt/app-root/bin/wp core download
    # Verify Checksums
    /opt/app-root/bin/wp core verify-checksums

    # Copy gitlab provided plugins, themes files into the persistent volume. 
    echo "Copying custom Themes and Plugins"
    if [ -d /opt/app-root/downloads/plugins ]; then
        cp -rf /opt/app-root/downloads/plugins/* wp-content/plugins/ 2>/dev/null || true
    fi
    if [ -d /opt/app-root/downloads/themes ]; then
        cp -rf /opt/app-root/downloads/themes/* wp-content/themes/ 2>/dev/null || true
    fi

    echo "Wordpress configuration files"
    # --skip-check avoids the need for mysql binary and mysql lcoal install
    /opt/app-root/bin/wp config create --dbname=$MYSQL_DATABASE --dbhost=$MYSQL_HOST --dbprefix=$MYSQL_TABLE_PREFIX --dbuser=$MYSQL_USER --dbpass=$MYSQL_PASSWORD --skip-check
    # Enforse SSL
    /opt/app-root/bin/wp config set FORCE_SSL_ADMIN true --raw
    # Fix http to https endless loop crap impossible to replace in the DB
    sed -i "/'FORCE_SSL_ADMIN', *true *);/a\
    if ( isset( \$_SERVER['HTTP_X_FORWARDED_PROTO'] ) && \$_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') \$_SERVER['HTTPS']='on';" wp-config.php

    echo "Wordpress site setup"
    # Ensure the DB is up and running
    until /opt/app-root/bin/wp db check
    do
        echo "Waiting for database connection..."
        # wait for 5 seconds before check again
        sleep 5
    done

    # wait for 10 seconds before continuing
    echo "Waiting 10s..."
    sleep 10

    # Create Wordpress instance and admin account
    /opt/app-root/bin/wp core install --url=https://$APPLICATION_NAME.web.cern.ch --title=$APPLICATION_NAME --admin_user=admin --admin_email=$ADMIN_EMAIL
    # Create Admin Users and enable Plugin
    #/opt/app-root/bin/wp user create admin $ADMIN_EMAIL --role=administrator
    # Enable english language
    # /opt/app-root/bin/wp language core install en_US
    # DEPRECATED /opt/app-root/bin/wp language core activate en_US
    /opt/app-root/bin/wp site switch-language en_US
    # Enable CERN SSO plugin which maps email attribute to WP internal DB
    /opt/app-root/bin/wp plugin activate cern-sso-authentication
    # Enable TwentySeventeen theme which is better than default theme
    #/opt/app-root/bin/wp theme activate twentyseventeen
    
    # Prevent SSL loop errors due to bad includes on http - DOES NOT WORK
    #/opt/app-root/bin/wp search-replace 'http://$APPLICATION_NAME.web.cern.ch' 'https://$APPLICATION_NAME.web.cern.ch' --skip-columns=guid --dry-run
    
    # Remove ping_sites like pingomatic (settings - writing - update services)
    wp option delete ping_sites
    
    # Update plugins
    /opt/app-root/bin/wp plugin update --all

    # Send Welcome Mail
    /opt/app-root/bin/msmtp/welcome.sh $ADMIN_EMAIL

else
    ###################### Update install if needed ######################
    echo "Wordpress site is already setup according to wp core is-installed command"
    #if ! $(/opt/app-root/bin/wp core check-update); then
    if ! [[ $(/opt/app-root/bin/wp core check-update) == *"WordPress is at the latest version"* ]] 2>/dev/null; then
        echo "Update is needed, proceeding"
        # Update WP core files
        /opt/app-root/bin/wp core update
        # Update DB eventually
        /opt/app-root/bin/wp core update-db
        # Verify Checksums
        /opt/app-root/bin/wp core verify-checksums
    fi
    # Update plugins
    /opt/app-root/bin/wp plugin update --all
fi

# Execute the original run script, replacing this script as current process.
exec /usr/libexec/s2i/run
