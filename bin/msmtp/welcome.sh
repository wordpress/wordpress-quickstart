#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/app-root/bin/msmtp
cat /opt/app-root/bin/msmtp/welcome-mail.eml | /opt/app-root/bin/msmtp/msmtp --host=cernmx.cern.ch --read-envelope-from $1
