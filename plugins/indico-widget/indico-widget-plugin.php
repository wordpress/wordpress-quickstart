<?php
/*
Plugin Name: Indico Widget Plugin
Plugin URI: http://gitlab.cern.ch/wordpress/indico-widget/
Description: This plugin adds an Indico widget displaying events from a specific category, or bookings for a specific room.
Version: 1.1
Author: Emmanuel Ormancey
Author URI: http://gitlab.cern.ch/ormancey/
License: GPLv3
*/

// Updater from https://github.com/YahnisElsts/plugin-update-checker
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = new Puc_v4p6_Vcs_PluginUpdateChecker(
    new Puc_v4p6_Vcs_GitLabApi('https://gitlab.cern.ch/wordpress/indico-widget/'),
    __FILE__,
    'indico-widget'
);
//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('stable-release');

// The widget class
class Indico_Widget extends WP_Widget {

	// Main constructor
	public function __construct() {
		parent::__construct(
			'indico_widget',
			__( 'Indico Widget', 'text_domain' ),
			array(
				'customize_selective_refresh' => true,
			)
		);
	}

	// The widget form (for the backend )
	public function form( $instance ) {

		// Set widget defaults
		$defaults = array(
			'title'    => 'Upcoming events',
			'text'     => '',
			'baseurl' => 'https://indico.cern.ch',
			'indicotype' => '',
			'indicoid' => '',			
			'parameters'   => '',
			'from' => 'today',
			'to' => 'today',
			'api_key' => '',
			'secret' => ''
		);
		
		// Parse current settings with defaults
		extract( wp_parse_args( ( array ) $instance, $defaults ) ); ?>

		<?php // Widget Title ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php _e( 'Widget Title', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<?php // Text Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>"><?php _e( 'Text:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'text' ) ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
		</p>

		<?php // baseurl Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'baseurl' ) ); ?>"><?php _e( 'Base Url:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'baseurl' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'baseurl' ) ); ?>" type="text" value="<?php echo esc_attr( $baseurl ); ?>" />
		</p>
		
		<?php // Dropdown indicotype ?>
		<p>
			<label for="<?php echo $this->get_field_id( 'indicotype' ); ?>"><?php _e( 'Type', 'text_domain' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'indicotype' ); ?>" id="<?php echo $this->get_field_id( 'indicotype' ); ?>" class="widefat">
			<?php
			// Your options array
			$options = array(
				''        => __( 'Type', 'text_domain' ),
				'category' => __( 'Events from a Category', 'text_domain' ),
				'room' => __( 'Room bookings', 'text_domain' ),
			);
			// Loop through options and add each one to the select dropdown
			foreach ( $options as $key => $name ) {
				echo '<option value="' . esc_attr( $key ) . '" id="' . esc_attr( $key ) . '" '. selected( $indicotype, $key, false ) . '>'. $name . '</option>';
			} ?>
			</select>
		</p>

		<?php // indicoid Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'indicoid' ) ); ?>"><?php _e( 'Category or Room ID:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'indicoid' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'indicoid' ) ); ?>" type="text" value="<?php echo esc_attr( $indicoid ); ?>" />
		</p>

		<?php // parameters Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'parameters' ) ); ?>"><?php _e( 'Additionnal Parameters:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'parameters' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'parameters' ) ); ?>" type="text" value="<?php echo esc_attr( $parameters ); ?>" />
		</p>

		<?php // from Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'from' ) ); ?>"><?php _e( 'From (<i>today</i> or <i>+x day</i> or -x day</i>):', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'from' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'from' ) ); ?>" type="text" value="<?php echo esc_attr( $from ); ?>" />
		</p>

		<?php // to Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'to' ) ); ?>"><?php _e( 'To (<i>today</i> or <i>+x day</i> or -x day</i>):', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'to' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'to' ) ); ?>" type="text" value="<?php echo esc_attr( $to ); ?>" />
		</p>

		<?php // api_key Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'api_key' ) ); ?>"><?php _e( 'API Key:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'api_key' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'api_key' ) ); ?>" type="text" value="<?php echo esc_attr( $api_key ); ?>" />
		</p>

		<?php // secret Field ?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'secret' ) ); ?>"><?php _e( 'Secret:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'secret' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'secret' ) ); ?>" type="text" value="<?php echo esc_attr( $secret ); ?>" />
		</p>
	<?php }

	// Update widget settings
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title']    = isset( $new_instance['title'] ) ? wp_strip_all_tags( $new_instance['title'] ) : '';
		$instance['text']     = isset( $new_instance['text'] ) ? wp_strip_all_tags( $new_instance['text'] ) : '';

		$instance['baseurl']     = isset( $new_instance['baseurl'] ) ? wp_strip_all_tags( $new_instance['baseurl'] ) : '';
		$instance['indicotype']     = isset( $new_instance['indicotype'] ) ? wp_strip_all_tags( $new_instance['indicotype'] ) : '';		
		$instance['indicoid']     = isset( $new_instance['indicoid'] ) ? wp_strip_all_tags( $new_instance['indicoid'] ) : '';
		$instance['parameters']     = isset( $new_instance['parameters'] ) ? wp_strip_all_tags( $new_instance['parameters'] ) : '';
		$instance['from']     = isset( $new_instance['from'] ) ? wp_strip_all_tags( $new_instance['from'] ) : '';
		$instance['to']     = isset( $new_instance['to'] ) ? wp_strip_all_tags( $new_instance['to'] ) : '';
		$instance['api_key']     = isset( $new_instance['api_key'] ) ? wp_strip_all_tags( $new_instance['api_key'] ) : '';
		$instance['secret']     = isset( $new_instance['secret'] ) ? wp_strip_all_tags( $new_instance['secret'] ) : '';

		return $instance;
	}

	// Display the widget
	public function widget( $args, $instance ) {

		extract( $args );

		// Check the widget options
		$title    = isset( $instance['title'] ) ? apply_filters( 'widget_title', $instance['title'] ) : '';
		$text     = isset( $instance['text'] ) ? $instance['text'] : '';

		$baseurl = isset( $instance['baseurl'] ) ?$instance['baseurl'] : '';
		$indicotype = isset( $instance['indicotype'] ) ?$instance['indicotype'] : '';
		$indicoid = isset( $instance['indicoid'] ) ?$instance['indicoid'] : '';
		$parameters = isset( $instance['parameters'] ) ?$instance['parameters'] : '';
		$from = isset( $instance['from'] ) ?$instance['from'] : '';
		$to = isset( $instance['to'] ) ?$instance['to'] : '';
		$api_key = isset( $instance['api_key'] ) ?$instance['api_key'] : '';
		$secret = isset( $instance['secret'] ) ?$instance['secret'] : '';

		// Get data from Indico
		parse_str($parameters, $params);
		if ($from == 'today')
			$params['from'] = $from;
		else {
			$fromdate = new DateTime($from);
			$params['from'] = $fromdate->format('Y-m-d');
		}

		if ($to == 'today')
			$params['to'] = $to;
		else {
			$todate = new DateTime($to);
			$params['to'] = $todate->format('Y-m-d');
		}

		$params['pretty'] = 'yes';

		// Basepath from indico type
		$basepath = '/export';
		if ($indicotype == 'room') {
			$basepath = $basepath . '/room/CERN/' . $indicoid . '.json';
			$params['detail'] = 'reservations';
		} else if ($indicotype == 'category') {
			$basepath = $basepath . '/categ/' . $indicoid . '.json';
		} else {
			echo 'Error: invalid indico type or id selected.';
			return false;
		}

		$fulluri = $baseurl . build_indico_request($basepath, $params, $api_key, $secret);


		// WordPress core before_widget hook (always include )
		echo $before_widget;

		// Display the widget
		echo '<div class="widget-text wp_widget_plugin_box">';

			// Display widget title if defined
			if ( $title ) {
				echo $before_title . $title . $after_title;
			}

			// Display text field
			if ( $text ) {
				echo '<p>' . $text . '</p>';
			}

			// get json and parse 
			// https://pippinsplugins.com/using-wp_remote_get-to-parse-json-from-remote-apis/
			$request = wp_remote_get( $fulluri );
			if ( is_wp_error( $request ) ) {
				echo '<p>Error ' . $request->get_error_message() . '</p>';
				//return false; // Bail early
			} else {
				$jsonbody = wp_remote_retrieve_body( $request );
				$indicodata = json_decode( $jsonbody );

				if( ! empty( $indicodata ) ) {
		
					echo '<ul>';
					if ($indicotype == 'room') 
					{
						foreach( $indicodata->results as $result ) {
							foreach( $result->reservations as $reservation ) {
								echo '<li>';
								echo '<a href="' . esc_url( $reservation->bookingUrl ) . '">' . $reservation->reason . '</a>';
								echo '<br/><small>(booked by ' . $reservation->bookedForName . ')</small>';
								echo '</li>';
							}
						}
					}
					else if ($indicotype == 'category') 
					{
						foreach( $indicodata->results as $result ) {
							echo '<li>';
							echo '<a href="' . esc_url( $result->url ) . '">' . $result->title . '</a>';
							echo '<br/><small>' . $result->description . '</small>';
							echo '</li>';
						}
					}
					else 
					{
						echo 'Error: invalid indico type or id selected.';
					}

					echo '</ul>';

				} else {
					echo '<p>No upcoming Indico events.</p>';
				}
			}		
		echo '</div>';

		// WordPress core after_widget hook (always include )
		echo $after_widget;

	}

}

// Register the widget
function register_indico_widget() {
	register_widget( 'Indico_Widget' );
}
add_action( 'widgets_init', 'register_indico_widget' );




function build_indico_request($path, $params, $api_key = null, $secret_key = null, $only_public = false, $persistent = false) {
    if($api_key) {
        $params['apikey'] = $api_key;
    }

    if($only_public) {
        $params['onlypublic'] = 'yes';
    }

    if($secret_key) {
        if(!$persistent) {
            $params['timestamp'] = time();
        }
        uksort($params, 'strcasecmp');
        $url = $path . '?' . http_build_query($params);
        $params['signature'] = hash_hmac('sha1', $url, $secret_key);
    }

    if(!$params) {
        return $path;
    }

    return $path . '?' . http_build_query($params);
}
